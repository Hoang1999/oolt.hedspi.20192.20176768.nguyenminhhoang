package aimsProject;
import java.util.Calendar;

import java.util.Calendar;

public class Order {
	public static final int MAX_NUMBERS_ORDERED=10;
	private DigitalVideoDisc itemsOrdered[]=new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered=0;//start from 1, not from 0
	private Calendar dateOrdered;
	public static final int MAX_LIMITTED_ORDERS=5;
	private static int nOrders=0;

	public Order() {
		nOrders=nOrders+1;
		 if(nOrders<MAX_LIMITTED_ORDERS)
			 System.out.println("nbOrder is below to the MAX_LIMITTED_ORDERS");
		 else if(nOrders==MAX_LIMITTED_ORDERS)
			 System.out.println("nbOrder have reached the MAX_LIMITTED_ORDERS");
		 else System.out.println("nbOrder have exceeded the MAX_LIMITTED_ORDERS");
		 
		 dateOrdered = Calendar.getInstance();
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		System.out.println("Adding dvd name "+disc.getTitle()+"...");
		if(qtyOrdered>=MAX_NUMBERS_ORDERED) {
			new WarningAndError().ErrorFull();
		}else {
			itemsOrdered[qtyOrdered]=disc;
			System.out.println("The disc name "+disc.getTitle()+" had been added");
			qtyOrdered=qtyOrdered+1;
			if(qtyOrdered>=MAX_NUMBERS_ORDERED) {
				new WarningAndError().WarningFull();
			}
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList) {
		//condition of dvdList:
		//+) dvdList have to be ranged
		
		int dvdListLength;
		dvdListLength=0;
		while(dvdList[dvdListLength]!=null) dvdListLength=dvdListLength+1;
		
		System.out.println("The list of dvd have "+dvdListLength+" dvd and you have "+(MAX_NUMBERS_ORDERED-qtyOrdered)+" slot left");
		if((dvdListLength+qtyOrdered)>MAX_NUMBERS_ORDERED) {
			System.out.println("Error: Your order cannot obtain all your list because of not having enough slot!");
		}
		
		System.out.println("Start adding...");
		int addingDvd;
		for(addingDvd=0; addingDvd<dvdListLength && qtyOrdered<MAX_NUMBERS_ORDERED; addingDvd++){
			System.out.println("Adding dvd name "+dvdList[addingDvd].getTitle()+"...");
			itemsOrdered[qtyOrdered]=dvdList[addingDvd];
			qtyOrdered=qtyOrdered+1;
			}
		System.out.println("Adding finished...");
		if(addingDvd+1<dvdListLength) {
			System.out.println("You have "+(dvdListLength-addingDvd-1)+" dvd in you list haven't added in this order:");
			int dvdLeft;
			for(dvdLeft=addingDvd; dvdLeft<dvdListLength; dvdLeft++) {
				System.out.println(dvdList[dvdLeft].getTitle());
			}
		}
		
		if(qtyOrdered>=MAX_NUMBERS_ORDERED) {
			new WarningAndError().WarningFull();
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2) {
		System.out.println("Adding dvd name "+dvd1.getTitle()+"...");
		if(qtyOrdered>=MAX_NUMBERS_ORDERED) {
			new WarningAndError().ErrorFull();
			System.out.println("Can not add dvd name "+dvd1.getTitle());
		}else {
			itemsOrdered[qtyOrdered]=dvd1;
			System.out.println("The disc name "+dvd1.getTitle()+" has been added");
			qtyOrdered=qtyOrdered+1;
			if(qtyOrdered>=MAX_NUMBERS_ORDERED) {
				new WarningAndError().WarningFull();
			}
		}
		
		System.out.println("Adding dvd name "+dvd2.getTitle()+"...");
		if(qtyOrdered>=MAX_NUMBERS_ORDERED) {
			new WarningAndError().ErrorFull();
			System.out.println("Can not add dvd name "+dvd2.getTitle());
		}else {
			itemsOrdered[qtyOrdered]=dvd2;
			System.out.println("The disc name "+dvd2.getTitle()+" has been added");
			qtyOrdered=qtyOrdered+1;
			if(qtyOrdered>=MAX_NUMBERS_ORDERED) {
				new WarningAndError().WarningFull();
			}
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int i;
		int flag;
		i=0;
		flag=0;
		
		while((i<qtyOrdered)&&(flag==0)) {
			if(disc.getTitle().equals(itemsOrdered[i].getTitle())==true)
				if(disc.getCategory().equals(itemsOrdered[i].getCategory())==true)
					if(disc.getDirector().equals(itemsOrdered[i].getDirector())==true)
						if(disc.getLength()==itemsOrdered[i].getLength())
							if(disc.getCost()==itemsOrdered[i].getCost()) {
								itemsOrdered[i]=null;
								flag=1;
							}
			if(flag==0) i=i+1;
		}

		if(i>=qtyOrdered) {
			System.out.println("This disc doesn't exist in the list");
		}else {
			//reorder the list
			for(i=i+1; i<qtyOrdered; i++)
				itemsOrdered[i-1]=itemsOrdered[i];
			itemsOrdered[i]=null;
			qtyOrdered=qtyOrdered-1;
		}
	}
	
	public float totalCost() {
		float total=0;
		int i;
		for(i=0; i<qtyOrdered; i++)
			total=total+itemsOrdered[i].getCost();
		return total;
	}
	
	public void printOrder() {
		int i;
		System.out.println("***********************************Order***********************************");
		System.out.println(dateOrdered.getTime());
		System.out.println();
		System.out.println("Ordered Items:");
		for(i=0; i<qtyOrdered; i++) {
			System.out.println((i+1)+". DVD"+"-"+
		itemsOrdered[i].getTitle()+"-"+
					itemsOrdered[i].getCategory()+"-"+
		itemsOrdered[i].getDirector()+"-"+
					itemsOrdered[i].getLength()+"-"+
		itemsOrdered[i].getCost());
			System.out.println();
		}
		System.out.println("Total cost: "+totalCost());
		System.out.println("***************************************************************************");
	}
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public Calendar getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Calendar dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
}
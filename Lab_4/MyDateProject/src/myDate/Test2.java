package myDate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Test2 {

	public static void main(String[] args) throws java.text.ParseException{
		// TODO Auto-generated method stub
		//TEST SORTING A NUMBERS OF DATE
		System.out.println("\n*****************************TEST SORTING A NUMBERS OF DATE*******************************\n");
		DateUtils dateUtils=new DateUtils();
		
		int length=5;
		Date[] date=new Date[length];
		String sDate1="31/12/1998";  
		Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
		String sDate2="30/12/1998";  
		Date date2=new SimpleDateFormat("dd/MM/yyyy").parse(sDate2);
		date[0]=date1;
		date[1]=date2;
		String sDate3="30/11/1998";  
		Date date3=new SimpleDateFormat("dd/MM/yyyy").parse(sDate3);
		date[2]=date3;
		String sDate4="30/01/1990";  
		Date date4=new SimpleDateFormat("dd/MM/yyyy").parse(sDate4);
		date[3]=date4;
		String sDate5="2/12/1998";  
		Date date5=new SimpleDateFormat("dd/MM/yyyy").parse(sDate5);
		date[4]=date5;
		
		dateUtils.compareArrayDates(date, length);
		for(int i=0; i<length; i++){
			System.out.println(DateFormat.getInstance().format(date[i]));
		}		
	}

}

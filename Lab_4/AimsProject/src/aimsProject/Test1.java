package aimsProject;

public class Test1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//TEST 1*************************************************************************************************
		Order anOrder=new Order();
		//Create a new dvd object and set the fields
		DigitalVideoDisc dvd1= new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Amimation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		DigitalVideoDisc dvd2= new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
		DigitalVideoDisc dvd3= new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		
		//add the dvd to the order
		anOrder.addDigitalVideoDisc(dvd1);
		anOrder.addDigitalVideoDisc(dvd2);
		anOrder.addDigitalVideoDisc(dvd3);
		anOrder.printOrder();
		
		//*******************************************************************************
		
		//remove dvd2&3
		anOrder.removeDigitalVideoDisc(dvd2);
		anOrder.removeDigitalVideoDisc(dvd3);
		anOrder.printOrder();
		//********************************************************************************
	}

}

package myDate;
import java.util.Calendar;
import java.util.Scanner;

public class MyDate {
	private int day;
	private int month;
	private int year;
	
	String[] monthInString= new String[]{"x", "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
	
	public MyDate() {
		Calendar calendar = Calendar.getInstance();
		this.day=calendar.get(Calendar.DATE);
		//Month in calendar start from 0
		this.month=calendar.get(Calendar.MONTH)+1;
		this.year=calendar.get(Calendar.YEAR);
	}
	
	public MyDate(int day, int month, int year) {
		if((year<=0)||((month<=0)||(month>12))||((day<=0)||(day>(new DayOfMonth(month,year).getMaxDay()))))
			System.out.println("Invalid Date!");
		else {
			this.day=day;
			this.month=month;
			this.year=year;
		}
	}
	
	//Work to do: have to check valid date
	public MyDate(String date) {
		dateInStringToDate(date);
	}
	
	public void accept() {
		String date;
		Scanner scan=new Scanner(System.in);
		System.out.print("Input date (EX: February 18th 2019): ");
		date=scan.nextLine();
		dateInStringToDate(date);
	}
	
	private void dateInStringToDate(String date) {
		//February 18th 2019
		date=date.trim();
		String[] dateInString=date.split("\\s");//splits the string based on whitespace 
		
		int i;
		i=1;
		String monthInLowerCase=dateInString[0].toLowerCase();
		while((i<=12)&&(monthInLowerCase.equals(monthInString[i])==false))
			i=i+1;

		if(i>12) {
			System.out.println("Invalid date!");
			return;
		}else this.month=i;
		
		//work to do: check valid of day
		i=0;
		while((dateInString[1].charAt(i)>='0')&&(dateInString[1].charAt(i)<='9'))
			i=i+1;
		this.day=Integer.parseInt(dateInString[1].substring(0, i));
		
		try {			
			this.year=Integer.parseInt(dateInString[2]);
		}catch(Exception se){
			System.out.println("Invalid date!");
			return;
		}
		
		if((this.year<=0)||((this.month<=0)||(this.month>12))||((this.day<=0)||(this.day>(new DayOfMonth(this.month,this.year).getMaxDay())))) {
			System.out.println("Invalid Date!");
			this.day=0;
			this.month=0;
			this.year=0;
		}
	}
	
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if((day<=0)||(day>31))
			System.out.println("Invalid day!");
		else
			this.day = day;
	}
	public int getMonth() {
		
		return month;
	}
	public void setMonth(int month) {
		if((month<=0)||(month>12))
			System.out.println("Invalid month!");
		else
			this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		if(year<=0)
			System.out.println("Invalid year!");
		else
			this.year = year;
	}
	
	public void print() {
		if((this.year<=0)||((this.month<=0)||(this.month>12))||((this.day<=0)||(this.day>(new DayOfMonth(this.month,this.year).getMaxDay())))) 
			System.out.println("NULL");	
		else System.out.println("Current date is: "+this.day+"-"+this.month+"-"+this.year);
	}
}

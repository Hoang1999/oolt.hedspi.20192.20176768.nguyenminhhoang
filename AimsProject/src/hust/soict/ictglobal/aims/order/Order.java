package hust.soict.ictglobal.aims.order;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.warningAndError.WarningAndError;

public class Order {
	//public static enum typeOfOrder{BOOK, DVD};
	
	private static int count=0;
	public final int id;
	public String orderName;
	//public typeOfOrder orderType;
	
	public static final int MAX_LIMITTED_ORDERS=5;
	private static int nOrders=0;
	
	public static final int MAX_NUMBERS_ORDERED=10;
	private List<Media> itemsOrdered=new ArrayList<Media>();
	
	private Calendar dateOrdered;

	public Order() {
		count=count+1;
		this.id=count;
		dateOrdered = Calendar.getInstance();
	}
	
	public Order(String orderName) {
		this();
		this.orderName=orderName;
		//this.orderType=orderType;
	}
	
	public boolean isEmpty() {
		return itemsOrdered.isEmpty();
	}

	public void addMedia(Media media) {
		System.out.println("Adding media name "+media.getTitle()+"...");
		if(itemsOrdered.size()>=MAX_NUMBERS_ORDERED) {
			new WarningAndError().ErrorFull();
		}else if(itemsOrdered.contains(media)==true)
			System.out.println("The item already exists!");
		else{
			itemsOrdered.add(media);
			System.out.println("The media name "+media.getTitle()+" had been added");
			if(itemsOrdered.size()>=MAX_NUMBERS_ORDERED) {
				new WarningAndError().WarningFull();
			}
		}
	}
	
	public void addMedia(Media media1, Media media2) {
		this.addMedia(media1);
		this.addMedia(media2);
	}
	
	public void addMedia(List<Media> mediaList) {
		System.out.println("The media list have "+mediaList.size()+" dvd and you have "+(MAX_NUMBERS_ORDERED-itemsOrdered.size())+" slot left");
		if((mediaList.size()+itemsOrdered.size())>MAX_NUMBERS_ORDERED) {
			System.out.println("Error: Your order cannot obtain all your list because of not having enough slot!");
		}
		
		System.out.println("Start adding...");
		int addingDvd;
		for(addingDvd=0; addingDvd<mediaList.size() && itemsOrdered.size()<MAX_NUMBERS_ORDERED; addingDvd++){
				this.addMedia(mediaList.get(addingDvd));
			}
		System.out.println("Adding finished...");
		
		if(addingDvd+1<mediaList.size()) {
			System.out.println("You have "+(mediaList.size()-addingDvd-1)+" dvd in you list haven't added in this order:");
			int dvdLeft;
			for(dvdLeft=addingDvd; dvdLeft<mediaList.size(); dvdLeft++) {
				System.out.println(mediaList.get(dvdLeft).getTitle());
			}
		}
		
		if(itemsOrdered.size()>=MAX_NUMBERS_ORDERED) {
			new WarningAndError().WarningFull();
		}
	}
	
	public void removeMedia(Media media) {
		boolean checkContain=itemsOrdered.contains(media);
		if(checkContain==false) {
			System.out.println("This media doesn't exist in the list");
			return;
		}else {
			itemsOrdered.remove(media);
			System.out.println("Successfully deleted the item");
		}
	}
	
	public void removeMedia(int id) {
		for(int i=0; i<itemsOrdered.size(); i++) {
			if(itemsOrdered.get(i).id==id) {
				this.removeMedia(itemsOrdered.get(i));
				return;
			}
		}
		System.out.println("No item have that id!");
	}
	
	public float totalCost() {
		float total=0;
		int i;
		for(i=0; i<itemsOrdered.size(); i++)
			total=total+itemsOrdered.get(i).getCost();
		return total;
	}
	
	public int printOrder() {
		if(itemsOrdered.isEmpty()==true) {
			System.out.println("The order is empty now!");
			return -1;
		}
		
		int i;
		System.out.println("***********************************Order***********************************");
		System.out.println(dateOrdered.getTime());
		System.out.println();
		System.out.println("Ordered Items:");
		for(i=0; i<itemsOrdered.size(); i++) {
			System.out.print("No."+i+": ");
			itemsOrdered.get(i).printMedia();
		}
		System.out.println("Total cost: "+totalCost());
		System.out.println("***************************************************************************");
		return 0;
	}
	
	public DigitalVideoDisc getALuckyItem() {
		int luckyNumber=(int)Math.random()*(itemsOrdered.size()-1);
		
		System.out.println("***********************************Lucky Item***********************************");
		System.out.println("Lucky Items:");
		System.out.println((luckyNumber+1)+".");
		itemsOrdered.get(luckyNumber).printMedia();
		System.out.println("Setting Item to be free...");
		itemsOrdered.get(luckyNumber).setCost(0);		
		System.out.println("***************************************************************************");
		printOrder();
		return (DigitalVideoDisc) itemsOrdered.get(luckyNumber);
	}
	
	//Getter and Setter
	public Calendar getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Calendar dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	
	public static int getnOrders() {
		return nOrders;
	}

	public static void setnOrders(int nOrders) {
		Order.nOrders = nOrders;
	}
}
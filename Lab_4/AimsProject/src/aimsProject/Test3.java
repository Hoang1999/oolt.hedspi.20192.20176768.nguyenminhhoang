package aimsProject;

public class Test3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//TEST3*************************************************************************************************
		Order anOrder3=new Order();
		
		DigitalVideoDisc[] dvdList=new DigitalVideoDisc[20]; 
		
		for(int i=0; i<9; i++) {
			dvdList[i]=new DigitalVideoDisc("dvd"+(i+1));
		}

		//add the dvdList to the order
		anOrder3.addDigitalVideoDisc(dvdList);
		
		DigitalVideoDisc dvd1= new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Amimation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		DigitalVideoDisc dvd2= new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);

		anOrder3.addDigitalVideoDisc(dvd1, dvd2);
		
		anOrder3.printOrder();
		//********************************************************************************		
	}

}

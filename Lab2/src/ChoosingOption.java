
import javax.swing.JOptionPane;

public class ChoosingOption {
	public static void main(String[] args) {
		Object[] options= {"YES", "NO"};
		int option=JOptionPane.showOptionDialog(null, "Do you want to change to the first class ticket?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
		
		JOptionPane.showMessageDialog(null,  "You've chosen: "+(option==JOptionPane.YES_NO_OPTION?"Yes":"No"));
		System.exit(0);
	}
}
/*file:///C:/Users/Administrator/Desktop/20192/OOP/TH_OOP/docs/api/javax/swing/JOptionPane.html*/
package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.PlayerException;

public class Track implements Playable, Comparable<Track>{
	private String title;
	private int length;
	
	public Track(String title) {
		this.title=title;
	}
	
	public Track(String title, int length) {
		this(title);
		this.length=length;
	}

	public int compareTo(Track obj) {
		return this.getTitle().compareToIgnoreCase(((Track)obj).getTitle());
	}
	
	public void play() throws PlayerException{
		if(this.getLength()<=0) {
			System.err.println("ERROR: TRACK length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing Track: "+this.getTitle());
		System.out.println("Track length: "+this.getLength());
	}

	public void printTrack() {
		System.out.println("Track name "+this.getTitle()+"-"+this.getLength());
		System.out.println();	
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
}

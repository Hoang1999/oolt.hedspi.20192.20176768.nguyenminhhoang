package hust.soict.ictglobal.garbage;

import java.util.Random;

public class ConcatenationInLoops {
	public static void main(String[] args) {
		//test the processing time to construct String using + operator
		Random r=new Random(123);
		long start =System.currentTimeMillis();
		String s="";
		for(int i=0; i<65536; i++)
			s+=r.nextInt(2);
		System.out.println("Using + operator processing time: "+(System.currentTimeMillis()-start));//This prints roughly 4500.
		
		//test the processing time to construct String using StringBuffer
		r=new Random(123);
		start=System.currentTimeMillis();
		StringBuffer sbf=new StringBuffer();
		for(int i=0; i<65536; i++)
			sbf.append(r.nextInt(2));
		s=sbf.toString();//toString() method returns the string representation of the object.
		System.out.println("Using StringBuffer processing time: "+(System.currentTimeMillis()-start));//This prints 5.
		
		//test the processing time to construct String using StringBuilder
		r=new Random(123);
		start=System.currentTimeMillis();
		StringBuilder sb=new StringBuilder();
		for(int i=0; i<65536; i++)
			sb.append(r.nextInt(2));
		s=sb.toString();//toString() method returns the string representation of the object.
		System.out.println("Using StringBuilder processing time: "+(System.currentTimeMillis()-start));
	}
}

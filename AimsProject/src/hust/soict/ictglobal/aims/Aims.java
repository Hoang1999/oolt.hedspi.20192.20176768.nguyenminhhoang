package hust.soict.ictglobal.aims;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.media.Track;
import hust.soict.ictglobal.aims.order.Order;

public class Aims {
	private static HashMap<String, Order> orderList=new HashMap<String, Order>();
	
	private static List<Book> bookItems=new ArrayList<Book>();
	private static List<DigitalVideoDisc> digitalVideoDiscItems=new ArrayList<DigitalVideoDisc>();
	private static List<CompactDisc> compactDiscItems=new ArrayList<CompactDisc>();
	private static List<Track> trackItems=new ArrayList<Track>();
	
	public static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	
	public static void showBookItemsList() {
		System.out.println("List of book items: ");
		for(int i=0; i<bookItems.size(); i++) {
			System.out.print("No."+i+":");
			bookItems.get(i).printMedia();
		}
	}

	public static void showDigitalVideoDiscItemsList() {
		System.out.println("List of DigitalVideoDisc items: ");
		for(int i=0; i<digitalVideoDiscItems.size(); i++) {
			System.out.println("No."+i+":");
			digitalVideoDiscItems.get(i).printMedia();
		}
	}

	public static void showCompactDiscItemsList() {
		System.out.println("List of compact disc items: ");
		for(int i=0; i<compactDiscItems.size(); i++) {
			System.out.println("No."+i+":");
			compactDiscItems.get(i).printMedia();
		}
	}
	
	public static void showTrackItemsList() {
		System.out.println("List of track items: ");
		for(int i=0; i<trackItems.size(); i++) {
			System.out.print("No."+i+":");
			trackItems.get(i).printTrack();
		}
	}

	private static void createNewOrder() {
		Scanner scan=new Scanner(System.in);

		System.out.println("Start create new order: ");
		if(Order.getnOrders()>=Order.MAX_LIMITTED_ORDERS)
			System.out.println("Number of order exceeded limit!");
			else {
				System.out.print("Enter new order name: ");
				String orderName=scan.nextLine();
				
				if(orderList.containsKey(orderName)==true)
					System.out.println("This order name already exists!");
				else {
					Order newOrder=new Order(orderName);
					orderList.put(orderName, newOrder);
					Order.setnOrders(orderList.size());
					System.out.println("New order had been added!");
				}
			}
	}
	
	private static void addBookItems(Order order) {
		Scanner scan=new Scanner(System.in);
		System.out.println("--------------------------------");
		System.out.println("How do you like to add: ");
		System.out.println("1. Add a single item");
		System.out.println("2. Add a list of items");
		System.out.println("0. Cancel");
		
		String choiceAddItems =new String();
		System.out.println("--------------------------------");
		System.out.print("Enter your choice: ");
		choiceAddItems=scan.nextLine();
		System.out.println("--------------------------------");
		
		if(choiceAddItems.length()<=1) {
			switch(choiceAddItems.charAt(0)) {
			case '1':
				System.out.print("Enter the number of item which you want to add: ");
				int addNo=scan.nextInt();
				scan.nextLine();
				if(addNo>=bookItems.size()||addNo<0) {
					System.out.print("Invalid!");
					break;
				}else {
					order.addMedia(bookItems.get(addNo));
				}
				break;
				
			case '2':
				System.out.print("Enter the number of start number: ");
				int addS=scan.nextInt();
				scan.nextLine();
				System.out.print("Enter the number of end number: ");
				int addE=scan.nextInt();
				scan.nextLine();
				
				if(addS>=bookItems.size()||addS<0||addE>=bookItems.size()||addE<0) {
					System.out.print("Invalid!");
					break;
				}else {
					List<Media> addList=new ArrayList<Media>();
					for(int i=addS; i<=addE; i++) {
						addList.add(bookItems.get(i));
					}
					order.addMedia(addList);
				}
				break;
			case '0':
				break;
				
			default:
				System.out.println("Invalid choice!");
				break;
			}
		}else System.out.println("Invalid choice!");
	}

	private static void addDigitalVideoDiscItems(Order order) {
		Scanner scan=new Scanner(System.in);
		System.out.println("--------------------------------");
		System.out.println("How do you like to add: ");
		System.out.println("1. Add a single item");
		System.out.println("2. Add a list of items");
		System.out.println("0. Cancel");
		
		String choiceAddItems =new String();
		System.out.println("--------------------------------");
		System.out.print("Enter your choice: ");
		choiceAddItems=scan.nextLine();
		System.out.println("--------------------------------");
		
		if(choiceAddItems.length()<=1) {
			switch(choiceAddItems.charAt(0)) {
			case '1':
				System.out.println("--------------------------------");
				System.out.print("Do you want to play item before add? (y/n): ");
				if(scan.nextLine().toLowerCase().contentEquals("y")) {
					int num=0;
					do{
						System.out.print("Enter the number of item which you want to play (-1 to exit playing): ");
						num=scan.nextInt();
						scan.nextLine();
						if(num!=-1) {
							try {
								digitalVideoDiscItems.get(num).play();
							}catch(PlayerException e) {
								e.printStackTrace();
							}
						}
					}while(num!=-1);
				}
				System.out.println("--------------------------------");
				
				System.out.print("Enter the number of item which you want to add: ");
				int addNo=scan.nextInt();
				scan.nextLine();
				if(addNo>=digitalVideoDiscItems.size()||addNo<0) {
					System.out.print("Invalid!");
					break;
				}else {
					order.addMedia(digitalVideoDiscItems.get(addNo));
				}
				break;
				
			case '2':
				System.out.print("Enter the number of start number: ");
				int addS=scan.nextInt();
				scan.nextLine();
				System.out.print("Enter the number of end number: ");
				int addE=scan.nextInt();
				scan.nextLine();
				
				if(addS>=digitalVideoDiscItems.size()||addS<0||addE>=digitalVideoDiscItems.size()||addE<0) {
					System.out.print("Invalid!");
					break;
				}else {
					List<Media> addList=new ArrayList<Media>();
					for(int i=addS; i<=addE; i++) {
						addList.add(digitalVideoDiscItems.get(i));
					}
					order.addMedia(addList);
				}
				break;
			case '0':
				break;
				
			default:
				System.out.println("Invalid choice!");
				break;
			}
		}else System.out.println("Invalid choice!");
	}

	private static void addCompactDiscItems(Order order) {
		Scanner scan=new Scanner(System.in);
		System.out.println("--------------------------------");
		System.out.println("How do you like to add: ");
		System.out.println("1. Add a single item");
		System.out.println("2. Add a list of items");
		System.out.println("0. Cancel");
		
		String choiceAddItems =new String();
		System.out.println("--------------------------------");
		System.out.print("Enter your choice: ");
		choiceAddItems=scan.nextLine();
		System.out.println("--------------------------------");
		
		if(choiceAddItems.length()<=1) {
			switch(choiceAddItems.charAt(0)) {
			case '1':
				System.out.println("--------------------------------");
					System.out.print("Do you want to play item before add? (y/n): ");
					if(scan.nextLine().toLowerCase().contentEquals("y")) {
						int num=0;
						do{
							System.out.print("Enter the number of item which you want to play (-1 to exit playing): ");
							num=scan.nextInt();
							scan.nextLine();
							if(num!=-1) {
								try {
								compactDiscItems.get(num).play();
								}catch(PlayerException e) {
									e.printStackTrace();
								}
							}
						}while(num!=-1);
					}
					System.out.println("--------------------------------");
				
				System.out.print("Enter the number of item which you want to add: ");
				int addNo=scan.nextInt();
				scan.nextLine();
				if(addNo>=compactDiscItems.size()||addNo<0) {
					System.out.print("Invalid!");
					break;
				}else {
					CompactDisc compactDisc=compactDiscItems.get(addNo);
					order.addMedia(compactDisc);
					System.out.println("--------------------------------");
					System.out.print("Do you want to add information for Tracks in your compactDisc?(y/n): ");
					if(scan.nextLine().toLowerCase().contentEquals("y")) {
						showTrackItemsList();
						addTrackItems(compactDisc);
					}
					System.out.println("--------------------------------");
				}
				break;
				
			case '2':
				System.out.print("Enter the number of start number: ");
				int addS=scan.nextInt();
				scan.nextLine();
				System.out.print("Enter the number of end number: ");
				int addE=scan.nextInt();
				scan.nextLine();
				
				if(addS>=compactDiscItems.size()||addS<0||addE>=compactDiscItems.size()||addE<0) {
					System.out.print("Invalid!");
					break;
				}else {
					List<Media> addList=new ArrayList<Media>();
					for(int i=addS; i<=addE; i++) {
						addList.add(compactDiscItems.get(i));
					}
					order.addMedia(addList);
				}
				break;
			case '0':
				break;
				
			default:
				System.out.println("Invalid choice!");
				break;
			}
		}else System.out.println("Invalid choice!");
	}

	private static void addTrackItems(CompactDisc compactDisc) {
		Scanner scan=new Scanner(System.in);
		System.out.println("--------------------------------");
		System.out.println("How do you like to add: ");
		System.out.println("1. Add a single track");
		System.out.println("2. Add a list of tracks");
		System.out.println("0. Cancel");
		
		String choiceAddTrack =new String();
		System.out.println("--------------------------------");
		System.out.print("Enter your choice: ");
		choiceAddTrack=scan.nextLine();
		System.out.println("--------------------------------");
		
		if(choiceAddTrack.length()<=1) {
			switch(choiceAddTrack.charAt(0)) {
			case '1':
				System.out.print("Enter the number of track which you want to add: ");
				int addNo=scan.nextInt();
				scan.nextLine();
				if(addNo>=trackItems.size()||addNo<0) {
					System.out.print("Invalid!");
					break;
				}else {
					compactDisc.addTrack(trackItems.get(addNo));
				}
				break;
				
			case '2':
				System.out.print("Enter the number of start number: ");
				int addS=scan.nextInt();
				scan.nextLine();
				System.out.print("Enter the number of end number: ");
				int addE=scan.nextInt();
				scan.nextLine();
				
				if(addS>=trackItems.size()||addS<0||addE>=trackItems.size()||addE<0) {
					System.out.print("Invalid!");
					break;
				}else {
					List<Track> addList=new ArrayList<Track>();
					for(int i=addS; i<=addE; i++) {
						addList.add(trackItems.get(i));
					}
					compactDisc.addTrack(addList);
				}
				break;
			case '0':
				break;
				
			default:
				System.out.println("Invalid choice!");
				break;
			}
		}else System.out.println("Invalid choice!");
	}
	
	private static Order getOrder(HashMap<String, Order> orderList) {
		Scanner scan=new Scanner(System.in);
		Order order=null;
		System.out.print("Enter order name: ");
		if((order=orderList.get(scan.nextLine()))==null) {
			System.out.println("Order doesn't exitst!");
			return null;
		}else return order;
	}
	
	public static void main(String[] args) {
		MemoryDaemon memoryDaemon=new MemoryDaemon();
		Thread newThread=new Thread(memoryDaemon);
		newThread.setDaemon(true);
		newThread.start();
		
		for(int i=0; i<30; i++) {
			Book newBook=new Book("Book"+i);
			bookItems.add(newBook);
		}
		
		for(int i=0; i<30; i++) {
			DigitalVideoDisc newDigitalVideoDisc=new DigitalVideoDisc("DigitalVideoDisc"+i);
			digitalVideoDiscItems.add(newDigitalVideoDisc);
		}
		
		for(int i=0; i<30; i++) {
			CompactDisc newCompactDisc=new CompactDisc("CompactDisc"+i);
			compactDiscItems.add(newCompactDisc);
		}
		
		for(int i=0; i<30; i++) {
			Track newTrack=new Track("Track"+i);
			trackItems.add(newTrack);
		}
		
		String choice =new String();
		Order order=null;
		do {
			showMenu();
			System.out.println("--------------------------------");
			System.out.print("Enter your choice: ");
			Scanner scan=new Scanner(System.in);
			choice=scan.nextLine();
			System.out.println("--------------------------------");
			
			if(choice.length()<=1) {
				switch(choice.charAt(0)) {
				case '1':
					createNewOrder();
					break;
					
				case '2':
					if((order=getOrder(orderList))==null) break;
					
					System.out.println("Enter type of items you want to add: ");
					System.out.println("1. DigitalVideoDisc");
					System.out.println("2. Book");
					System.out.println("3. CompactDisc");
					System.out.println("0. Cancel");
					
					String choiceItemType =new String();
					System.out.println("--------------------------------");
					System.out.print("Enter your choice: ");
					choiceItemType=scan.nextLine();
					System.out.println("--------------------------------");
					
					if(choiceItemType.length()<=1) {							
						switch(choiceItemType.charAt(0)) {
						case '1':
							showDigitalVideoDiscItemsList();
							addDigitalVideoDiscItems(order);
							break;
							
						case '2':
							showBookItemsList();
							addBookItems(order);
							break;
							
						case '3':
							showCompactDiscItemsList();
							addCompactDiscItems(order);
							break;
							
						case '0':
							System.out.println("Exit");
							break;
							
						default:
							System.out.println("Invalid choice!");
							break;
						}
					}else System.out.println("Invalid choice!");
					break;
					
				case '3':
					if((order=getOrder(orderList))==null) break;
					if(order.printOrder()==-1) break;
					System.out.print("Enter id of item: ");
					order.removeMedia(scan.nextInt());
					scan.nextLine();
					break;
					
				case '4':
					if((order=getOrder(orderList))==null) break;
					order.printOrder();
					break;
					
					/*
				case '5'://test
					HashMap<Integer, String> test=new HashMap<Integer, String>();
					test.put(1, "");
					test.put(10, "");
					test.put(3, "");
					 for(Map.Entry m:test.entrySet()){    
				           System.out.println(m.getKey()+" \\ "+(int)m.hashCode());    
				          }  
					test.remove(10);
					for(Map.Entry m:test.entrySet()){    
				           System.out.println(m.getKey()+" \\ "+m.hashCode());    
				          }  
					break;*/
					
				case '0':
					System.out.println("Exit!");
					break;
				default:
					System.out.println("Invalid choice!");
					break;
				}
			}else System.out.println("Invalid choice!");
			System.out.println("--------------------------------");
		}while(choice.toString().equals("0")==false);
	}
}
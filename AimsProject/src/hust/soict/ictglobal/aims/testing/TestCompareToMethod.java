package hust.soict.ictglobal.aims.testing;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;

public class TestCompareToMethod {

	public static void main(String args[]) {
		java.util.List collection=new java.util.ArrayList();
		
		//add the DVD objects to the ArrayList
		DigitalVideoDisc dvd1=new DigitalVideoDisc("The Lion King", " ", " ", 87, 0);

		DigitalVideoDisc dvd2=new DigitalVideoDisc("Star Wars", " ", " ", 124, 0);

		DigitalVideoDisc dvd3=new DigitalVideoDisc("Aladdin", " ", " ", 90, 1);
		collection.add(dvd2);
		collection.add(dvd1);
		collection.add(dvd3);
		
		//Iterate through the ArrayList and output their titles
		//(unsorted order)
		java.util.Iterator iterator=collection.iterator();
		
		System.out.println("-------------------------------");
		System.out.println("The DVDs currently in the order are: ");
		
		while(iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		
		//Sort the collection of DVDs - based on the compareTo() method
		java.util.Collections.sort((java.util.List)collection);
		
		//Iterate through the ArrayList and output their titles - in sorted order
		iterator=collection.iterator();
		
		System.out.println("--------------------------------");
		System.out.println("The DVDs in sorted order are: ");
		
		while(iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		
		System.out.println("--------------------------------");
	}

}

package hust.soict.ictglobal.aims.testing;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order;

public class DiskTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Order anOrder=new Order();
		//Create a new dvd object and set the fields
		DigitalVideoDisc dvd1= new DigitalVideoDisc("Harry Potter");
		dvd1.setCategory("Fiction");
		dvd1.setCost(19.95f);
		dvd1.setDirector("JK Rowling");
		dvd1.setLength(87);
		
		DigitalVideoDisc dvd2= new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
		DigitalVideoDisc dvd3= new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		
		anOrder.addMedia(dvd1);
		anOrder.addMedia(dvd2);
		anOrder.addMedia(dvd3);
		anOrder.printOrder();
		
		System.out.println(dvd1.search("Harry Potter"));
		System.out.println(dvd1.search("Potter Harry"));
		System.out.println(dvd1.search("Potter"));
		
		System.out.println(dvd1.search("Aladdin"));
		System.out.println(dvd3.search("Aladdin"));
		
		anOrder.getALuckyItem();
	}

}

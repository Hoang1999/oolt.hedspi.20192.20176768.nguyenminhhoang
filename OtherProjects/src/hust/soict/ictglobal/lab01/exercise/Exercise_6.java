package hust.soict.ictglobal.lab01.exercise;
import java.util.*;

class FirstDegreeEquationWithOneVariable{
	double a;
	double b;
	double c;
	//ax+b=c
	
	double x;
	
	FirstDegreeEquationWithOneVariable(){
		Scanner scan=new Scanner(System.in);
		System.out.print("a=");
		a=scan.nextDouble();
		System.out.print("b=");
		b=scan.nextDouble();
		System.out.print("c=");
		c=scan.nextDouble();
		
		System.out.println("\n"+a+"x + "+b+" = "+c+"\n");
		
		scan.close();
	}
	
	void solve() {
		if(a==0) {
			if(c==b) System.out.println("With all x");
			else System.out.println("Vo nghiem");
		}else System.out.println("x = "+((c-b)/a));
	}
}

class FirstDegreeEquationWithTwoVariable{
	double a;
	double b;
	double c;
	//ax+by=c
	
	double x;
	double y;
	
	FirstDegreeEquationWithTwoVariable(){
		Scanner scan=new Scanner(System.in);
		
		System.out.print("a=");
		a=scan.nextDouble();
		System.out.print("b=");
		b=scan.nextDouble();
		System.out.print("c=");
		c=scan.nextDouble();
		
		System.out.println("\n"+a+"x + "+b+"y = "+c+"\n");
		
		scan.close();
	}
	
	void solve() {
		if((a==0)&&(b==0)) {
			if(c==0) System.out.println("With all y and x");
			else System.out.println("Vo nghiem");
		}else if(a==0) {
			y=c/b;
			System.out.println("With all x and y="+y);
		}else if(b==0) {
			x=c/a;
			System.out.println("With all y and x="+x);
		}else {
			System.out.println("x=(c-by)/a");
		}
	}
}

class SecondDegreeEquationWithOneVariable{
	double a;
	double b;
	double c;
	//ax2+bx+c=0
	
	double x;
	
	SecondDegreeEquationWithOneVariable(){
		Scanner scan=new Scanner(System.in);
		
		System.out.print("a=");
		a=scan.nextDouble();
		System.out.print("b=");
		b=scan.nextDouble();
		System.out.print("c=");
		c=scan.nextDouble();
		
		System.out.println("\n"+a+"x2 + "+b+"x + "+c+" = 0\n");
		
		scan.close();
	}
	
	void solve() {
	if(a==0) {
		if(b==0) {
			if(c==0) System.out.println("With all x");
			else System.out.println("Vo nghiem");
		}else System.out.println("x = "+((0-c)/b));
	}else {
		double delta=b*b-4*a*c;
		if(delta<0) System.out.println("Vo nghiem");
		else if (delta==0) System.out.println("x = "+((0-b)/(2*a)));
		else {
			double deltaSqrt=Math.sqrt(delta);
			System.out.println("x1 = "+(((0-b)-deltaSqrt)/(2*a)));
			System.out.println("x1 = "+(((0-b)+deltaSqrt)/(2*a)));
			}
		}
	}
}

public class Exercise_6 {
	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		
		System.out.println("1: The first degree equation with one variable");
		System.out.println("2: The first degree equation with two variables");
		System.out.println("3: The second degree equation with one variable");
		System.out.println("*****************************************************");
		System.out.print("Please choose option:");
		int option;
		option=scan.nextInt();
		System.out.println("*****************************************************");
		switch(option) {
		case 1:
			new FirstDegreeEquationWithOneVariable().solve();
			break;
		case 2:
			new FirstDegreeEquationWithTwoVariable().solve();
			break;
		case 3:
			new SecondDegreeEquationWithOneVariable().solve();
			break;
		default:
			System.out.println("Option Invalid");
			break;
		}
		
		scan.close();
	}	
}

package hust.soict.ictglobal.garbage;

import java.io.FileReader;
import java.io.IOException;

public class NoGarbage {
	public static void main(String[] args) throws IOException {

		//read file*************************************************************************
		String fileName="input.txt";
		ClassLoader classLoader=new NoGarbage().getClass().getClassLoader();
		FileReader fr=new FileReader(classLoader.getResource(fileName).getFile());
		//**********************************************************************************
		
		
		int i;
		int garbageCount=-1;
		
		long start =System.currentTimeMillis();
		String s="";
		StringBuffer sbf=new StringBuffer();
		
		while((i=fr.read())!=-1) {
			sbf.append((char)i);
			garbageCount=garbageCount+1;
		}
		s=sbf.toString();//toString() method returns the string representation of the object.
		System.out.println("Using + operator processing time: "+(System.currentTimeMillis()-start));
		fr.close();
	}
}

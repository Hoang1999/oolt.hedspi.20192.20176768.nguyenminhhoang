package hust.soict.ictglobal.date.testing;

import hust.soict.ictglobal.date.myDate.MyDate;

public class Test3 {

	public static void main(String[] args) throws java.text.ParseException {
		// TODO Auto-generated method stub
		//TEST PRINT
		System.out.println("\n******************************************TEST PRINT******************************************\n");
		//Form 1: Date Format using getInstance(): 31/3/15 2:37 PM
		//Form 2: Date Format using getDateInstance(): 31 Mar, 2015
		//Form 3: Date Format using getTimeInstance(): 2:37:23 PM
		//Form 4: Date Format using getDateTimeInstance(): 31 Mar, 2015 2:37:23 PM
		//Form 5: Date Format using getTimeInstance(DateFormat.SHORT): 2:37 PM
		//Form 6: Date Format using getTimeInstance(DateFormat.MEDIUM): 2:37:23 PM
		//Form 7: Date Format using getTimeInstance(DateFormat.LONG): 2:37:23 PM IST
		//Form 8: Date Format using getDateTimeInstance(DateFormat.LONG,DateFormat.SHORT): 31 March, 2015 2:37 PM
		
		MyDate testDatePrintFormat=new MyDate();
		testDatePrintFormat.print(1);
		testDatePrintFormat.print(2);
		testDatePrintFormat.print(3);
		testDatePrintFormat.print(4);
		testDatePrintFormat.print(5);
		testDatePrintFormat.print(6);
		testDatePrintFormat.print(7);
		testDatePrintFormat.print(8);		
	}

}

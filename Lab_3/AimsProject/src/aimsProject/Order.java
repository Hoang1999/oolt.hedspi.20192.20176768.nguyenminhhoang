package aimsProject;

public class Order {
	public static final int MAX_NUMBERS_ORDERED=10;
	private DigitalVideoDisc itemsOrdered[]=new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrdered=0;//start from 1, not from 0
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered>=MAX_NUMBERS_ORDERED) {
			System.out.println("The order is already full");
		}else {
			itemsOrdered[qtyOrdered]=disc;
			System.out.println("The disc has been added");
			qtyOrdered=qtyOrdered+1;
		}
		
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		int i;
		int flag;
		i=0;
		flag=0;
		
		while((i<qtyOrdered)&&(flag==0)) {
			if(disc.getTitle().equals(itemsOrdered[i].getTitle())==true)
				if(disc.getCategory().equals(itemsOrdered[i].getCategory())==true)
					if(disc.getDirector().equals(itemsOrdered[i].getDirector())==true)
						if(disc.getLength()==itemsOrdered[i].getLength())
							if(disc.getCost()==itemsOrdered[i].getCost()) {
								itemsOrdered[i]=null;
								flag=1;
							}
			if(flag==0) i=i+1;
		}

		if(i>=qtyOrdered) {
			System.out.println("This disc doesn't exist in the list");
		}else {
			//reorder the list
			for(i=i+1; i<qtyOrdered; i++)
				itemsOrdered[i-1]=itemsOrdered[i];
			itemsOrdered[i]=null;
			qtyOrdered=qtyOrdered-1;
		}
	}
	
	public float totalCost() {
		float total=0;
		int i;
		for(i=0; i<qtyOrdered; i++)
			total=total+itemsOrdered[i].getCost();
		return total;
	}
	
	public void printOrder() {
		int i;
		for(i=0; i<qtyOrdered; i++)
			System.out.println(itemsOrdered[i].getTitle());
	}
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
}

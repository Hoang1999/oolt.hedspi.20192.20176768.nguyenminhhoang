package hust.soict.ictglobal.aims.testing;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Order;

public class Test1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//TEST 1*************************************************************************************************
		Order anOrder=new Order();
		//Create a new dvd object and set the fields
		DigitalVideoDisc dvd1= new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Amimation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		
		DigitalVideoDisc dvd2= new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		
		DigitalVideoDisc dvd3= new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		
		//add the dvd to the order
		anOrder.addMedia(dvd1);
		anOrder.addMedia(dvd2);
		anOrder.addMedia(dvd3);
		anOrder.printOrder();
		
		//*******************************************************************************
		
		//remove dvd2&3
		anOrder.removeMedia(dvd2);
		anOrder.removeMedia(dvd3);
		anOrder.printOrder();
		//********************************************************************************
	}

}

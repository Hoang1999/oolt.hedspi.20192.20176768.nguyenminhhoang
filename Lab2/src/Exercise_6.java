
import java.util.*;

class NumericArray{
	int arrayLength;
	int array[];
	int arraySum;
	double arrayAvg;
	
	NumericArray(){
		Scanner scan=new Scanner(System.in);
		System.out.print("Array length: ");
		this.arrayLength=scan.nextInt();
		array=new int[arrayLength];
	}
	
	NumericArray(int arrayLength) {
		this.arrayLength=arrayLength;
		array=new int[this.arrayLength];
	}
	
	void inputArrayKeyboard() {
		Scanner scan=new Scanner(System.in);
		for(int i=0; i<arrayLength; i++) {
			System.out.print("E["+i+"]=");
			array[i]=scan.nextInt();
		}
	}
	
	void sumArray() {
		arraySum=0;
		for(int i=0; i<arrayLength; i++) 
			arraySum=arraySum+array[i];
	}
	
	void avgArray() {
		arrayAvg=(double)arraySum/arrayLength;
	}
	
	void sortG() {
		for(int i=0; i<arrayLength-1; i++)
			for(int j=arrayLength-1; j>i; j--)
				for(int k=i; k<arrayLength-1; k++)
					if(array[k]>array[k+1]) {
						int tmp;
						tmp=array[k];
						array[k]=array[k+1];
						array[k+1]=tmp;
					}
	}
	
	void sortL() {
		for(int i=0; i<arrayLength-1; i++)
			for(int j=arrayLength-1; j>i; j--)
				for(int k=i; k<arrayLength-1; k++)
					if(array[k]<array[k+1]) {
						int tmp;
						tmp=array[k];
						array[k]=array[k+1];
						array[k+1]=tmp;
					}
	}
	
	void printArray() {
		System.out.println(Arrays.toString(array));
	}
}

public class Exercise_6 {
	public static void main(String[] args) {
		NumericArray array=new NumericArray();
		array.inputArrayKeyboard();
		array.sortG();
		System.out.println("");
		array.printArray();
		array.sortL();
		System.out.println("");
		array.printArray();
		array.sumArray();
		array.avgArray();
		System.out.println("");
		System.out.println("Sum="+array.arraySum);
		System.out.println("");
		System.out.println("Avg="+array.arrayAvg);
	}
}

package hust.soict.ictglobal.garbage;
import java.io.FileWriter; 

public class CreateRandomText {
	public static void main (String[] args) throws Exception {
		//read file*************************************************************************
		String fileName="input.txt";
		ClassLoader classLoader=new NoGarbage().getClass().getClassLoader();
		FileWriter fw=new FileWriter(classLoader.getResource(fileName).getFile());
		//**********************************************************************************		
		
		for(int i=0; i<65536; i++)
			fw.write("M");
		fw.close();		
	}
}

package hust.soict.ictglobal.date.myDate;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import hust.soict.ictglobal.date.convert_SYear_To_Year.Convert_SYear_To_Year;
import hust.soict.ictglobal.date.dayOfMonth.DayOfMonth;

public class MyDate {
	private int day;
	private int month;
	private int year;
	
	String[] monthInString= new String[]{"x", "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
	String[] dayInString=new String[] {"x", "first", "second", "third","fourth","fifth","sixth","seventh","eighth","ninth","tenth",
			"eleventh","twelfth","thirteenth","fourteenth","fifteenth","sixteenth","seventeenth","eighteenth","nineteenth",
			"twentieth","twenty first","twenty second","twenty third","twenty fourth","twenty fifth","twenty sixth","twenty seventh",
			"twenty eighth","twenty ninth","thirtieth","thirty first"};
	
	public MyDate() {
		Calendar calendar = Calendar.getInstance();
		this.day=calendar.get(Calendar.DATE);
		//Month in calendar start from 0
		this.month=calendar.get(Calendar.MONTH)+1;
		this.year=calendar.get(Calendar.YEAR);
	}
	
	public MyDate(int day, int month, int year) {
		if((year<=0)||((month<=0)||(month>12))||((day<=0)||(day>(new DayOfMonth(month,year).getMaxDay()))))
			System.out.println("Invalid Date!");
		else {
			this.day=day;
			this.month=month;
			this.year=year;
		}
	}
	
	//Work to do: have to check valid date
	public MyDate(String date) {
		dateInStringToDate(date);
	}
	
	public void accept() {
		String date;
		Scanner scan=new Scanner(System.in);
		System.out.print("Input date (EX: February 18th 2019): ");
		date=scan.nextLine();
		dateInStringToDate(date);
	}
	
	private void dateInStringToDate(String date) {
		//February 18th 2019
		date=date.trim();
		String[] dateInString=date.split("\\s");//splits the string based on whitespace 
		
		int i;
		i=1;
		String monthInLowerCase=dateInString[0].toLowerCase();
		while((i<=12)&&(monthInLowerCase.equals(monthInString[i])==false))
			i=i+1;

		if(i>12) {
			System.out.println("Invalid date!");
			return;
		}else this.month=i;
		
		//work to do: check valid of day
		i=0;
		while((dateInString[1].charAt(i)>='0')&&(dateInString[1].charAt(i)<='9'))
			i=i+1;
		this.day=Integer.parseInt(dateInString[1].substring(0, i));
		
		try {			
			this.year=Integer.parseInt(dateInString[2]);
		}catch(Exception se){
			System.out.println("Invalid date!");
			return;
		}
		
		if((this.year<=0)||((this.month<=0)||(this.month>12))||((this.day<=0)||(this.day>(new DayOfMonth(this.month,this.year).getMaxDay())))) {
			System.out.println("Invalid Date!");
			this.day=0;
			this.month=0;
			this.year=0;
		}
	}
	
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if((day<=0)||(day>31))
			System.out.println("Invalid day!");
		else
			this.day = day;
	}
	public void setDay(String sDay) {
		String sDayInString=sDay.toLowerCase();
		int i=0;
		while((i<=31)&&(sDayInString.equals(dayInString[i])==false))
			i=i+1;

		if(i>31) {
			System.out.println("Invalid day!");
			return;
		}else this.day=i;
	}
	public int getMonth() {
		
		return month;
	}
	public void setMonth(int month) {
		if((month<=0)||(month>12))
			System.out.println("Invalid month!");
		else
			this.month = month;
	}
	public void setMonth(String sMonth) {
		String sMmonthInString=sMonth.toLowerCase();
		int i=0;
		while((i<=12)&&(sMmonthInString.equals(monthInString[i])==false))
			i=i+1;

		if(i>12) {
			System.out.println("Invalid month!");
			return;
		}else this.month=i;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		if(year<=0)
			System.out.println("Invalid year!");
		else
			this.year = year;
	}
	public void setYear(String sYear) {
		int year=new Convert_SYear_To_Year().convertStringToYear(sYear);
		if(year<=0)
			System.out.println("Invalid year!");
		else
			this.year = year;
	}
	
	public void print() {
		if((this.year<=0)||((this.month<=0)||(this.month>12))||((this.day<=0)||(this.day>(new DayOfMonth(this.month,this.year).getMaxDay())))) 
			System.out.println("NULL");	
		else System.out.println("Current date is: "+this.day+"-"+this.month+"-"+this.year);
	}
	
	public void printForm() {
		System.out.println("Form 1: Date Format using getInstance(): 31/3/15 2:37 PM");
		System.out.println("Form 2: Date Format using getDateInstance(): 31 Mar, 2015");
		System.out.println("Form 3: Date Format using getTimeInstance(): 2:37:23 PM");
		System.out.println("Form 4: Date Format using getDateTimeInstance(): 31 Mar, 2015 2:37:23 PM");
		System.out.println("Form 5: Date Format using getTimeInstance(DateFormat.SHORT): 2:37 PM");
		System.out.println("Form 6: Date Format using getTimeInstance(DateFormat.MEDIUM): 2:37:23 PM");
		System.out.println("Form 7: Date Format using getTimeInstance(DateFormat.LONG): 2:37:23 PM IST");
		System.out.println("Form 8: Date Format using getDateTimeInstance(DateFormat.LONG,DateFormat.SHORT): 31 March, 2015 2:37 PM");
	}
	
	public void print(int choice) throws java.text.ParseException{
		String sDate="";
		if(day<10) sDate=sDate+"0"+day;
		else sDate=sDate+day;
		if(month<10) sDate=sDate+"/0"+month;
		else sDate=sDate+"/"+month;
		sDate=sDate+"/"+year;
		
		Date date=new SimpleDateFormat("dd/MM/yyyy").parse(sDate);
		
		 String dateToStr;
		 
		 switch(choice) {
		 case 1:
			 dateToStr = DateFormat.getInstance().format(date);  
			 System.out.println("Current date is: "+dateToStr); 
			 break;
		 case 2:
			 dateToStr = DateFormat.getDateInstance().format(date);  
		     System.out.println("Current date is: "+dateToStr);
			 break;
		 case 3:
			 dateToStr = DateFormat.getTimeInstance().format(date);  
		        System.out.println("Current date is: "+dateToStr);
			 break;
		 case 4:
			 dateToStr = DateFormat.getDateTimeInstance().format(date);  
		        System.out.println("Current date is: "+dateToStr);
			 break;
		 case 5:
			 dateToStr = DateFormat.getTimeInstance(DateFormat.SHORT).format(date);  
		        System.out.println("Current date is: "+dateToStr);
			 break;
		 case 6:
			 dateToStr = DateFormat.getTimeInstance(DateFormat.MEDIUM).format(date);  
		        System.out.println("Current date is: "+dateToStr);
			 break;
		 case 7:
			 dateToStr = DateFormat.getTimeInstance(DateFormat.LONG).format(date);  
		        System.out.println("Current date is: "+dateToStr); 
			 break;
		 case 8:
			 dateToStr = DateFormat.getDateTimeInstance(DateFormat.LONG,DateFormat.SHORT).format(date);  
		        System.out.println("Current date is: "+dateToStr);
			 break;
		 default:
				break;
		 }
	}
}

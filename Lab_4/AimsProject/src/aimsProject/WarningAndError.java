package aimsProject;

public class WarningAndError {
	public void WarningFull() {
		System.out.println("Warning: The order is full now!\n\tPlease make sure to have enough slot if you want to add more!");
	}
	
	public void ErrorFull() {
		System.out.println("Error: The order is already full!");
	}
}

package hust.soict.ictglobal.garbage;
import java.io.FileReader; 

public class GarbageCreator {
	public static void main(String[] args) throws Exception{
		//read file*************************************************************************
		String fileName="input.txt";
		ClassLoader classLoader=new NoGarbage().getClass().getClassLoader();
		FileReader fr=new FileReader(classLoader.getResource(fileName).getFile());
		//**********************************************************************************
		
		
		int i;
		int garbageCount=-1;
		
		long start =System.currentTimeMillis();
		String s="";
		while((i=fr.read())!=-1) {
			s=s+(char)i;
			garbageCount=garbageCount+1;
			if(garbageCount>=50000) {
				System.out.println(">=1000 unused String objects had been created! Stop!");
				break;
			}
		}
		System.out.println("Using + operator processing time: "+(System.currentTimeMillis()-start));
		fr.close();
	}
}

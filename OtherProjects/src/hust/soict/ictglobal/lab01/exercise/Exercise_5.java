package hust.soict.ictglobal.lab01.exercise;
import java.util.*;

class Caculation{
	double num1;
	double num2;
	
	Caculation(double num1, double num2){
		this.num1=num1;
		this.num2=num2;
	}
	
	double sum() {
		return num1+num2;
	}
	
	double minus() {
		return num1-num2;
	}
	
	double product() {
		return num1*num2;
	}
	
	double quotient() {
		return num1/num2;
	}
}

public class Exercise_5 {
	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		
		double num1;
		double num2;
		
		System.out.print("First number:");
		num1=Double.parseDouble(scan.nextLine());
		
		System.out.print("Second number:");
		num2=Double.parseDouble(scan.nextLine());
		
		Caculation cal=new Caculation(num1, num2);
		
		System.out.println("Sum:"+cal.sum());
		System.out.println("Minus: "+cal.minus());
		System.out.println("Product: "+cal.product());
		if(num2!=0)
			System.out.println("Quotient: "+cal.quotient());
		else System.out.println("Quotient: Invalid");
		
		scan.close();
		}
	}

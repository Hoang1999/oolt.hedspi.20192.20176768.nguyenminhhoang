package hust.soict.ictglobal.aims.media;

public abstract class Media implements Comparable<Media>{
	private static int counter=0;
	public final int id;
	
	private String title;
	private String category;
	private float cost;
	
	private Media() {
		counter=counter+1;
		this.id=counter;
	}
	
	public Media(String title) {
		this();
		this.title=title;
	}
	
	public Media(String title, String category) {
		this(title);
		this.category=category;
	}
	
	public Media(String title, String category, float cost) {
		this(title, category);
		setCost(cost);
	}
	
	public boolean equals(Object anObject) throws NullPointerException, ClassCastException{
		boolean result=false;
		
		try{
			if((anObject instanceof Media)==false) {
				System.err.println("ERROR: Not the instance of media");
				throw (new ClassCastException());
			}
			if(this.title.equals(((Media)anObject).getTitle())&&(this.cost==((Media)anObject).getCost())){
				result=true;
				}else result=false;
	}catch(Exception e) {
		e.printStackTrace();
	}
		return result;
	}
	
	public int compareTo(Media aMedia) throws NullPointerException, ClassCastException{
		int result=-1;
		
		try{
			if(this.title.equals(aMedia.getTitle())&&(this.cost==aMedia.getCost())){
				result=0;
				}else if(this.title.equals(aMedia.getTitle())!=true){
					return this.title.compareTo(aMedia.getTitle());					
				}else if(this.cost>aMedia.getCost())
					return 1;
				else return -1;
	}catch(Exception e) {
		e.printStackTrace();
	}
		return result;
	}
	
	public void printMedia() {}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
}

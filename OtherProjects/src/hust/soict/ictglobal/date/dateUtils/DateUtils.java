package hust.soict.ictglobal.date.dateUtils;
import java.util.Date;  

public class DateUtils {
	public static int compareTwoDates(Date date1, Date date2) {  
        int comparison=date1.compareTo(date2);  
        //comparison==0 =>"Two date is equal"
        //comparison<0 =>date2 is greater than date1
        //else date1 is greater than date2
        return comparison;
	}
	
	public void getCompareTwoDates(Date date1, Date date2) {
		int comparison=compareTwoDates(date1, date2);
		if(comparison==0) System.out.println("Two date is equal");
		else if(comparison<0) System.out.println(date2+" is greater than "+date1);
        else System.out.println(date1+" is greater than "+date2);
	}
	
	public static void compareArrayDates(Date[] date, int length) {
		for(int i=0; i<length-1; i++)
			for(int j=length-1; j>i; j--)
				for(int k=i; k<length-1; k++)
					if(compareTwoDates(date[k],date[k+1])>0) {
						Date tmp;
						tmp=date[k];
						date[k]=date[k+1];
						date[k+1]=tmp;
					}
	}
}

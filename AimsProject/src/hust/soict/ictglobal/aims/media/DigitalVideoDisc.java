package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable{
	public DigitalVideoDisc(String title){
		super(title);
	}
	
	public DigitalVideoDisc(String title, String category){
		super(title, category);
	}
	
	public DigitalVideoDisc(String title, String category, String director){
		super(title, category, director);
	}
	
	public DigitalVideoDisc(String title, String category, String director, int length, float cost){
		super(title, category, director, length, cost);
	}
	
	public int compareTo(DigitalVideoDisc obj) {
		if(this.getCost()>((DigitalVideoDisc)obj).getCost())
			return 1;
		else if(this.getCost()<((DigitalVideoDisc)obj).getCost())
			return -1;
		else return this.getTitle().compareToIgnoreCase(((DigitalVideoDisc)obj).getTitle());
	}
	
	public boolean search(String title) {
		boolean contain=true;
		int flag=0;
		
		title=title.toLowerCase();
		String[] titleWord=title.split("\\s");
		String[] diskTitleWord=getTitle().toLowerCase().split("\\s");
		
		int diskTitleLength=diskTitleWord.length;
		int findingTitleLength=titleWord.length;
		
		//if title and this.title have different length???????????????????????????????????
		//Case that title is "one one" and the disk's title is "one"???????????????????????
		
		int i=0;
		int j=0;
		while((flag!=findingTitleLength)&&(j<diskTitleLength)) {
			if(diskTitleWord[j]==null) j=j+1;
			else if(titleWord[i].equals(diskTitleWord[j])==true) {
				flag=flag+1;
				diskTitleWord[j]=null;
				i=i+1;
				j=0;
			}else j=j+1;
		}
		if(flag!=findingTitleLength) contain=false;
		return contain;
	}

	public void play() throws PlayerException{
		if(this.getLength()<=0) {
			System.err.println("ERROR: DVD length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing DVD: "+this.getTitle());
		System.out.println("DVD length: "+this.getLength());
	}

	public void printMedia() {
		System.out.println("Dvd id "+this.id+"-"+this.getTitle()+"-"+this.getCategory()+"-"+
				this.getDirector()+"-"+this.getLength()+"-"+this.getCost());
		System.out.println();
	}
}

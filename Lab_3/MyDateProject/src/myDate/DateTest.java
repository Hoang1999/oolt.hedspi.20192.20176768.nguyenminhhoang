package myDate;

public class DateTest {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MyDate testDate1=new MyDate();
		testDate1.print();
		
		MyDate testDate2=new MyDate(31, 12, 1999);
		testDate2.print();
		
		MyDate testDate3=new MyDate("febrUary 18th 2019");
		testDate3.print();
		
		//test invalid date
		MyDate testDate5=new MyDate(29, 2, 2019);
		testDate5.print();
		
		MyDate testDate6=new MyDate("ruary 18th 2019");
		testDate6.print();
		
		MyDate testDate7=new MyDate("February 18th -2019");
		testDate7.print();
		
		MyDate testDate4=new MyDate();
		testDate4.accept();
		testDate4.print();
	}

}

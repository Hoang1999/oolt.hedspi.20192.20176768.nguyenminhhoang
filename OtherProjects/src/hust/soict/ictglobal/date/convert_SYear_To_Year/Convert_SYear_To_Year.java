package hust.soict.ictglobal.date.convert_SYear_To_Year;

public class Convert_SYear_To_Year {
	
	public int convertSingleWord(String word) {
		String[] single_digits=new String[] {"zero", "one", "two",  
				"three", "four","five",  
				"six", "seven", "eight", "nine"};
		
		String[] two_digits=new String[]{"ten", "eleven", "twelve",  
                "thirteen", "fourteen", 
                "fifteen", "sixteen",  
                "seventeen", "eighteen", "nineteen"};
		
		String[] tens_multiple={"", "", "twenty", "thirty", "forty", "fifty", 
                "sixty", "seventy", "eighty", "ninety"};
		
		String[] tens_power={"hundred", "thousand"};
		
		int convertNum=0;
		
		if(word.equals("o")==true)
			return 100;
		
		int flag=0;
		for(int i=0; i<single_digits.length; i++) {
			if(word.equals(single_digits[i])==true){
				convertNum=i;
				flag=1;
			}
		}
		
		if(flag==0)
			for(int i=0; i<two_digits.length; i++) {
				if(word.equals(two_digits[i])==true){
					convertNum=i+10;
					flag=1;
				}
			}
		
		if(flag==0)
			for(int i=0; i<tens_multiple.length; i++) {
				if(word.equals(tens_multiple[i])==true){
					convertNum=i*10;
					flag=1;
				}
			}
		
		if(flag==0)
			for(int i=0; i<tens_power.length; i++) {
				if(word.equals(tens_power[i])==true){
					if(i==0) convertNum=100;
					else convertNum=1000;
					flag=1;
				}
			}
		
		if(flag==0) return -2;//CAN NOT TRANSFORM WORD TO NUMBER
		return convertNum;
	}
	
	public int convertStringToYear(String sYear) {
		System.out.println("Warning: year with length more than 4 is not supported");
		
		//Delete Not Used String( "and", "-")
		sYear.replace("-", " ");
		sYear.replace("and","");
		
		//trims()
		sYear.trim();
		
		String sYearLowerCase=sYear.toLowerCase();
		
		String[] yearInString=sYearLowerCase.split("\\s");
		
		int year=0;
		
		for(int i=0; i<yearInString.length; i++) {
			int num=convertSingleWord(yearInString[i]);
			if(num==-2) {
				return -1;
			}
			else if(num!=-1) {
				if(year==0) year=num;
				else if((year>0)&&(num==100|num==1000)) year=year*num;
				else if(year>0&&year<100) {
					if(year%10==0&&num<10) year=year+num;
					else year=year*100+num;
				}else if(year>=100) year=year+num;
			}
		}
		
		return year;
	}
}

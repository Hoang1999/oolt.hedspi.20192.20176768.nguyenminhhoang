package hust.soict.ictglobal.date.testing;

import java.text.SimpleDateFormat;
import java.util.Date;

import hust.soict.ictglobal.date.dateUtils.DateUtils;

public class Test1 {

	public static void main(String[] args) throws java.text.ParseException{
		// TODO Auto-generated method stub
		//TEST COMPARE TWO DATES
		System.out.println("\n*********************************TEST COMPARE TWO DATES************************************\n");
		DateUtils dateUtils=new DateUtils();

		String sDate1="31/12/1998";  
		Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
		String sDate2="30/12/1998";  
		Date date2=new SimpleDateFormat("dd/MM/yyyy").parse(sDate2);
		dateUtils.getCompareTwoDates(date1, date2);		
	}

}

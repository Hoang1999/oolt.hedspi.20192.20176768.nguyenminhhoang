package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.ictglobal.aims.PlayerException;

public class CompactDisc extends Disc  implements Playable{
	private String artist;
	private int length;//total length of track
	private List<Track> tracks=new ArrayList<Track>();
	
	public int compareTo(CompactDisc obj) {
		if(this.tracks.size()>((CompactDisc)obj).tracks.size())
			return 1;
		else if(this.tracks.size()<((CompactDisc)obj).tracks.size())
			return -1;
		else if(this.length>((CompactDisc)obj).length)
			return 1;
		else if(this.length<((CompactDisc)obj).length)
			return -1;
		else return this.getTitle().compareToIgnoreCase(((CompactDisc)obj).getTitle());
	}
	
	public void  addTrack(Track track) {
		System.out.println("Adding track name "+track.getTitle()+"...");
		if(tracks.contains(track)==true)
			System.out.println("The item already exists!");
		else{
			tracks.add(track);
			System.out.println("The track name "+track.getTitle()+" had been added");
			}
		}
	
	public void addTrack(List<Track> trackList) {
		System.out.println("Start adding...");
		int addingTrack;
		for(addingTrack=0; addingTrack<trackList.size(); addingTrack++){
				this.addTrack(trackList.get(addingTrack));
			}
		System.out.println("Adding finished...");
	}
	
	public void removeTrack(Track track) {
		boolean checkContain=tracks.contains(track);
		if(checkContain==false) {
			System.out.println("This track doesn't exist in the list");
			return;
		}else {
			tracks.remove(track);
			System.out.println("Successfully deleted the item");
		}
	} 
	
	public int getLength() {
		this.length=0;
		for(int i=0; i<tracks.size(); i++) {
			this.length=this.length+tracks.get(i).getLength();
		}
		
		return this.length;
	}
	
	public CompactDisc(String title){
		super(title);
	}
	
	public CompactDisc(String title, String category){
		super(title, category);
	}
	
	public CompactDisc(String title, String category, String director){
		super(title, category, director);
	}
	
	public CompactDisc(String title, String category, String director, int length, float cost){
		super(title, category, director, length, cost);
	}

	public void play() throws PlayerException{
		if(this.length<=0) {
			System.err.println("ERROR: CD length is 0");
			throw (new PlayerException());
		}
		
		System.out.println("Playing CD: "+this.getTitle());
		System.out.println("CD length: "+this.getLength());
		
		java.util.Iterator<Track> iter=tracks.iterator();
		Track nextTrack;
		
		while(iter.hasNext()) {
			nextTrack=(Track)iter.next();
			try {
				nextTrack.play();
			}catch (PlayerException e) {
				e.printStackTrace();
			}
		}
		for(int i=0; i<tracks.size(); i++) {
			tracks.get(i).play();
		}
	}

	public void printMedia() {
		Scanner scan=new Scanner(System.in);
		
		System.out.println("Compact Disc id "+this.id+"-"+this.getTitle()+"-"+this.getCategory()+"-"+
				this.getCost()+"-"+this.getArtist()+"-"+this.getLength());
		printTrack();
		System.out.println();
	}
	
	public void printTrack() {
		if(tracks.isEmpty()==true) {
			System.out.println("The Compact Disc is empty now!");
			return;
		}
		
		int i;
		System.out.println("***********************************Track***********************************");
		System.out.println("Tracks list:");
		for(i=0; i<tracks.size(); i++) {
			System.out.print("No."+i+": ");
			tracks.get(i).printTrack();
		}
		System.out.println("***************************************************************************");
		return;
	}
	
	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

}

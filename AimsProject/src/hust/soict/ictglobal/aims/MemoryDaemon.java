package hust.soict.ictglobal.aims;

public class MemoryDaemon implements Runnable{
	long memoryUsed=0;//Keeps track of the memory usage in the system
	
	//Create a loop which will log the amount of memory used as the Aims.main() method executes.
	//You will make use of the java.lang.Runtime class, which has a static method getRuntime().
	
	public void run() {
		Runtime rt=Runtime.getRuntime();//returns the singleton instance of Runtime class.
		long used;
		
		while(true) {//while(true) keep running until the user thread is terminated
			used=rt.totalMemory()-rt.freeMemory();
			if(used!=memoryUsed) {
				System.out.println("\n\tMemory used = "+used);
				memoryUsed=used;
			}
		}
	}
	
	public MemoryDaemon() {
		// TODO Auto-generated constructor stub
	}

}

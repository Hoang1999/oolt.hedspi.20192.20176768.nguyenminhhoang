package lab10.exercise6;

import java.text.SimpleDateFormat;
import java.util.Date;

import lab10.exercise6.exception.IllegalBirthDayException;
import lab10.exercise6.exception.IllegalGPAException;

public class Student{
	private String studentID=new String();
	private String studentName=new String();
	private Date date=new Date();
	SimpleDateFormat formatter=new SimpleDateFormat("dd/MM/yyyy");
	String strDate=formatter.format(date);
	private float gpa;
	
	public Student(String studentID, String studentName, String date, float gpa) throws IllegalGPAException, Exception{
		this.studentID=studentID;
		this.studentName=studentName;
		try {
			this.date=formatter.parse(date);
			if(this.date.getMonth()>12||this.date.getMonth()<1)
				throw(new IllegalBirthDayException());
			if(this.date.getDay()<1||this.date.getDay()>new DayOfMonth(this.date.getMonth(), this.date.getYear()).getMaxDay())
				throw(new IllegalBirthDayException());
		}catch(Exception e) {
			throw(new IllegalBirthDayException());
		}
		if(gpa>4||gpa<0)
			throw (new IllegalGPAException());
		this.gpa=gpa;
	}
	
	public String getStudentID() {
		return studentID;
	}
	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}

package hust.soict.ictglobal.aims.testing;

import java.util.ArrayList;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.order.Order;

public class Test2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//TEST2*************************************************************************************************
		Order anOrder2=new Order();
		//Create a new dvd object and set the fields
		DigitalVideoDisc dvd3anOrder2= new DigitalVideoDisc("Aladdin");
		dvd3anOrder2.setCategory("Animation");
		dvd3anOrder2.setCost(18.99f);
		dvd3anOrder2.setDirector("John Musker");
		dvd3anOrder2.setLength(90);
		
		//add the dvd to the order
		anOrder2.addMedia(dvd3anOrder2);
		
		ArrayList<Media> dvdList=new ArrayList<Media>(); 
		
		for(int i=0; i<13; i++) {
			dvdList.add(new DigitalVideoDisc("dvd"+(i+1)));
		}

		//add the dvdList to the order
		anOrder2.addMedia(dvdList);
		anOrder2.printOrder();
		//********************************************************************************		
	}

}

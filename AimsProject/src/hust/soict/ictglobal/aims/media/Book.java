package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;  

public class Book extends Media{
	private List<String> authors=new ArrayList<String>();
	private String content;
	private List<String> contentTokens=new ArrayList<String>();
	private Map<String, Integer> wordFrequency=new HashMap<String, Integer>();
	
	public Book(String title) {
		super(title);
	}

	public Book(String title, String category) {
		super(title, category);
	}
	
	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors=authors;
	}

	public void processContent() {
		contentTokens.removeAll(contentTokens);
		String replacedContent=content.replace(". ", " ");//replace punctuations to space character
		while(replacedContent.contains("  "))
			replacedContent=replacedContent.replace("  ", " ");//delete more than one backspace
		String[] tokens=replacedContent.split("\\s");
		for(int i=0; i<tokens.length; i++)
			contentTokens.add(tokens[i].toLowerCase());
		java.util.Collections.sort((List<String>)contentTokens);
			
		wordFrequency.clear();
		int count=1;
		int tokenIndex=0;
		for(int i=1; i<contentTokens.size(); i++) {
			if(contentTokens.get(i).equals(contentTokens.get(tokenIndex).toLowerCase())==true)
				count=count+1;
			else {
				wordFrequency.put(contentTokens.get(tokenIndex), count);
				count=1;
				tokenIndex=i;
			}
			if(i==contentTokens.size()-1) {
				wordFrequency.put(contentTokens.get(tokenIndex), count);
			}
		}
	}
	
	public String toString(){
		String stringReturn=new String();
		
		stringReturn="Authors: ";
		if(authors.size()==0);
		else {
			stringReturn=stringReturn+authors.get(0);
			for(int i=1; i<authors.size(); i++)
				stringReturn=stringReturn+", "+authors.get(i);
		}
		stringReturn=stringReturn+"\n";
		
		stringReturn=stringReturn+"Content: ";
		stringReturn=stringReturn+content+"\n";
		
		stringReturn=stringReturn+"Content length: ";
		stringReturn=stringReturn+contentTokens.size()+"\n";
		
		stringReturn=stringReturn+"Tokens list: ";
		stringReturn=stringReturn+contentTokens.get(0);
		for(int i=1; i<contentTokens.size(); i++)
			stringReturn=stringReturn+", "+contentTokens.get(i);
		stringReturn=stringReturn+"\n";
		
		stringReturn=stringReturn+"Word frequency: ";
		stringReturn=stringReturn+contentTokens.get(0)+"-"+wordFrequency.get(contentTokens.get(0));
		int tokenIndex=0;
		for(int i=1; i<contentTokens.size(); i++) {
			if(contentTokens.get(i).equals(contentTokens.get(tokenIndex))==false){
				tokenIndex=i;
				stringReturn=stringReturn+", "+contentTokens.get(tokenIndex)+"-"+wordFrequency.get(contentTokens.get(tokenIndex));
			}
		}
		return stringReturn;
	}
	
	public void printMedia() {
		System.out.println("Book id "+this.id+"-"+this.getTitle()+"-"+
				this.getCategory()+"-"+this.getCost()+"-");
		for(int j=0; j<this.authors.size(); j++) {
			System.out.println("-"+authors.get(j));
		}
		System.out.println();	
	}
	
	public void addAuthor(String authorName) {
		boolean checkContain=authors.contains(authorName);
		if(checkContain==true) {
			System.out.println("This authors already exists!");
			return;
		}else if(checkContain==false){
			System.out.println("Start adding...");
			authors.add(authorName);
			System.out.println("Adding "+authorName+" completed!");
		}
	}
	
	public void removeAuthor(String authorName) {
		boolean checkContain=authors.contains(authorName);
		if(checkContain==false) {
			System.out.println("This authors doesn't exists!");
			return;
		}else if(checkContain==true){
			System.out.println("Start deleting...");
			authors.remove(authorName);
			System.out.println("Removing "+authorName+" completed!");
		}
	}
	
	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		processContent();
	}
}
